/*****
*	This code applies magnetic force in 2D to a particle system
*	Locations of particles are translated into delay parameters
*	Dillon Bastan 2018
******/



/************************************************************/
/**********************  GLOBALS/INIT  **********************/

//Particle system class
include("MagnetEmitter");

//IO
inlets = 1;
outlets = 2;	//Parameters, Magnet Locations
//mgraphics
mgraphics.init();
mgraphics.relative_coords = 1;
mgraphics.autosketch = 0;
mgraphics.autofill = 0;
//Magnets
var magnet_pos = new Magnet({ dim:2, charge:1, mass:6, pos:[0,0.1], magnetPower:1 });
var magnet_neg = new Magnet({ dim:2, charge:-1, mass:6, pos:[0,-0.1], magnetPower:1 });
//Particle system
var systemArgs = {
	velocityLimit: 0.5,
	magnetLimit: 0.1,
	flowfieldStrength: 0,
	nparticles: 32
};
var system = new MagnetEmitter(systemArgs);
//Parameters
var maxDelayTime;
//UI Vars
var hitXY = -1;
var selectedMagnetIndex = -1;
var hitLocation;
//colors
var col_positive = [0.427, 0.843, 1.];
var col_negative = [1., 0.71, 0.196];
var col_background = [0.157, 0.157, 0.157, 1.];
var col_circles = [0.5, 0.5, 0.5, 1];

/************************************************/




/****************************************************/
/**********************  MAIN  **********************/

//
function bang() {
	//Don't run until initialized
	if(!system.activeStates.length)
		return;
	//Apply forces from both magnets on the particle system
	system.applyAttraction( magnet_pos );
	system.applyAttraction( magnet_neg );
	//Apply magnetic force to and from all particles
	system.attractSystem( system );
	//Calculate particle positions
	system.run();
	//Call the paint function, which displays objects
	mgraphics.redraw();
	//Convert particle locations to parameters and output them
	outputParameters( system );
}

//
function paint() {
	with (mgraphics) {
		//Background
		set_source_rgba( col_background );
		rectangle(-1, 1, 2, 2);
		fill();
		//Radial circles
		var r, halfR;
		set_source_rgba( col_circles );
		//Inner Circle
		r = 0.4;
		halfR = r * 0.5;
		ellipse(-halfR, halfR, r, r);
		//
		r = 1;
		halfR = r * 0.5;
		ellipse(-halfR, halfR, r, r);
		//Outer Circle
		r = 1.8;
		halfR = r * 0.5;
		ellipse(-halfR, halfR, r, r);
		//
		stroke();
	}
	//Display magnets
	magnet_pos.displaySquare(col_positive, col_negative);
	magnet_neg.displaySquare(col_positive, col_negative);
	//Display particles
	system.display2D(col_positive, col_negative);
}

/************************************************/




/********************************************************/
/**********************  FUNCTIONS  **********************/

//
function set_maxDelayTime(v) {
	maxDelayTime = v;
}

//Set the locations of the magnets
function set_magnetLocations(xPos, yPos, xNeg, yNeg) {
	magnet_pos.location.set([ xPos, yPos ]);
	magnet_neg.location.set([ xNeg, yNeg ]);
}

//Set the strength of the magnets
function set_magnetStrength(v) {
	magnet_pos.magnetPower = v;
	magnet_neg.magnetPower = v;
}

//Sets the limit of the magnetic force
function set_magnetLimit(v) {
	system.magnetLimit = v;
}

//Set the active states of the particles
function set_particleStates() {
	system.setStates( arrayfromargs(arguments) );
}

//Set the masses of the particles
function set_particleMasses() {
	system.setMasses( arrayfromargs(arguments) );
}

//Set the charges of the particles
function set_particleCharges() {
	system.setCharges( arrayfromargs(arguments) );
}

//Set the magnetic strength of the particles
function set_particleStrength(v) {
	system.setMagneticPowers( v );
}

//Set the velocity limit of the particles
function set_velocityLimit(v) {
	system.velocityLimit = v;
}

//Randomiz particle locations
function randomizeParticleLocations() {
	system.randomizeLocations();
}

//Convert particle locations to parameters and output them
function outputParameters( system ) {
	//
	var delayTimes = [];
	var filterCenters = [];
	var pans = [];
	var x = [];
	var y = [];
	for (var i = 0; i < system.particles.length; i++) {
		//Get position vector
		var pos = system.particles[i].location.dup();
		//Delay time is magnitude of position vector
		var dtime = pos.mag() / Math.sqrt(2);
		delayTimes[i] = Math.pow(dtime, 3) * maxDelayTime + 10;		//(0-1) to (10-2100)
		//
		var xy = pos.get();
		//Filter center is based on y position
		var center = xy[1]*0.5+0.5;
		filterCenters[i] = Math.pow(2, (center*119.589417 - 53.51318)/12) * 440;//(0-1) to (20-20k)
		//Panning is based on x position
		pans[i] = xy[0]*0.5+0.5;
		x [i] = xy[0];
		y [i] = xy[1];
		
	}
	//
	outlet(0, "delayTimes", delayTimes);
	outlet(0, "filterCenters", filterCenters);
	outlet(0, "pans", pans);
	outlet(0, "x", x);
	outlet(0, "y", y);
	
}

/************************************************/




/********************************************************/
/*********************  INTERFACE  ************************/

//Mouse click
function onclick(x, y) {
	//Store hit position
	hitXY = [ x/mgraphics.size[0]*2-1, -1*(y/mgraphics.size[1]*2-1) ];
	//Determine which magnet is underneath the mouse to move
	selectedMagnetIndex = objectUnderneath( hitXY, [ magnet_pos, magnet_neg ] );
	//Store initial location of magnet so we can drag it
	hitLocation = (selectedMagnetIndex === 0)? magnet_pos.location.dup() : magnet_neg.location.dup();
}
onclick.local = 1;


//Mouse Drag
function ondrag(x, y, button) {
	//Determine if mouse release
	if (button === 0) {
		onrelease();
		return;
	}
	//Determine if actually drag or just a click, or if no magnet is selected
	xy = [x/mgraphics.size[0]*2-1, -1*(y/mgraphics.size[1]*2-1) ];
	if ( hitXY[0] === xy[0] && hitXY[1] === xy[1] || selectedMagnetIndex === -1 )
		return;
	//Get delta of mouse from hit
	var deltaXY = new PVector([ xy[0]-hitXY[0], xy[1]-hitXY[1] ]);
	//Apply delta to the initial location of the magnet
	deltaXY.add( hitLocation );
	deltaXY.constrain(-1, 1);
	//Update
	switch (selectedMagnetIndex) {
		case 0:
			magnet_pos.location = deltaXY.dup();
			break;
		case 1:	
			magnet_neg.location = deltaXY.dup();
			break;
	}
	//Output locations for UI objects
	outlet(1, magnet_pos.location.get(), magnet_neg.location.get() );
}
ondrag.local = 1;


//Mouse Release
function onrelease() {
	//Reset
    hitXY = -1;
	selectedMagnetIndex = -1;
	hitLocation = -1;
}
onrelease.local = 1;


//Return index of which object is underneath the point
function objectUnderneath( pointXY, objects ) {
	var index = -1;
	for (var i = 0; i < objects.length; ++i) {
		//Get boundaries of object
		var radius = objects[i].mass * 0.05;	//this value based on how I display it
		var halfR = radius * 0.5;
		var xy = objects[i].location.get();
		xy[0] -= halfR;
		xy[1] += halfR;
		//Check if the point is within the boundaries
		if ( (pointXY[0] > xy[0] && pointXY[0] < xy[0]+radius) 
		&& (pointXY[1] < xy[1] && pointXY[1] > xy[1]-radius) )
			index = i;
	}
	return index;
}
objectUnderneath.local = 1;

/********************************************************/




/********************************************************/
/*********************  UTILITY  ************************/

//
function forcesize(w, h) {
	if (w!=h) {
		h = w;
		box.size(w,h);
	}
}
forcesize.local = 1;

//
function onresize(w,h) {
	forcesize(w, h);
	mgraphics.redraw();
}
onresize.local = 1; 

//
function calcAspect() {
	var width = this.box.rect[2] - this.box.rect[0];
	var height = this.box.rect[3] - this.box.rect[1];
	return width/height;
}
onresize.local = 1; 